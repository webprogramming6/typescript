class Person1 {
    public constructor(private readonly name: string) {
        this.name = name;
    }

    public getName(): string {
        return this.name;
    }
}

const person1 = new Person1("Jane");
console.log(person1);
console.log(person1.getName());

interface Shape {
    getArea: () => number;
}

class Rectangle2 implements Shape {
    public constructor(
        protected readonly width1: number,
        protected readonly height1: number
    ) { }

    public getArea(): number {
        return this.width1 * this.height1;
    }
}

const rectangle2 = new Rectangle2(10, 20);
console.log(rectangle2);
console.log(rectangle2.getArea());

class Square extends Rectangle2 {
    public constructor(width1: number) {
        super(width1, width1);
    }
    public toString(): string {
        return `Square [${this.width1}]`;
    }
}

const square = new Square(10);
console.log(square);
console.log(square.getArea());
console.log(square.toString());