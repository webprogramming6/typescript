//Return Type
function getTime(): number {
    return new Date().getTime();
}

console.log(getTime())

//Void Return Type
function printHello(): void {
    console.log("Hello")
}
printHello()

//Parameters
function multiply(a: number, b: number) {
    return a * b;
}
console.log(multiply(5, 10))

//Named Parameters
function divide({ a, b }: { a: number, b: number }) {
    return a / b
}
console.log(divide({ a: 100, b: 10 }))

//Optional Parameters
function add(a: number, b: number, c?: number) {
    return a + b + (c || 0);
}
console.log(add(5, 10, 5))
console.log(add(5, 10))

//Default Parameters
function pow(value: number, exponent: number = 10) {
    return value ** exponent;
}
console.log(pow(5, 10))

//Rest Parameters
function add2(a: number, b: number, ...rest: number[]) {
    return a + b + rest.reduce((p, c) => p + c, 0);
}
console.log(add2(1, 2, 3, 4, 5, 6, 7))

//Type Alias
// => is return
type Negate = (value: number) => number;
const negateFunction: Negate = (value) => value * -1;
const negateFunction2: Negate = function (value: number): number {
    return value * -1
};
console.log(negateFunction(1))
console.log(negateFunction2(1))