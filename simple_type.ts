//Explicit Type
let firstName: string = "Dylan";
console.log(firstName);

//Implicit Type
let lastName = "Landy";
console.log(lastName);

//Error
//firstName = 33;

//Type of
console.log(typeof firstName);