//Change Type
let x: unknown;
console.log(typeof x)
x = 'hello'
console.log(typeof x)
console.log((x as string).length);

//Casting with <>
let x2: unknown = 'hello';
console.log((<string>x2).length);

//Force casting
let x3 = 'hello';
console.log(((x3 as unknown) as number).toFixed);