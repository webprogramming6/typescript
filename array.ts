//Readonly keyword can prevent arrays from being changed.
//const names: readonly string[] = [];

const names: string[] = [];
names.push("Dylan");
names.push("Harry");
names.push("Fhang");
console.log(names);
//names.pop()
console.log(names);
console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++) {
    console.log(names[i]);
}

for (let i in names) {
    console.log(names[i]);
}

names.forEach(function (name) {
    console.log(name);
})