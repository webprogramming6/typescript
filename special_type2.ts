//Type : unknown
let w: unknown = 1;
w = "string"; // no error
w = {
    //Arrow method
    runANonExistentMethod: () => {
        console.log("I think therefore I am");
    }
} as { runANonExistentMethod: () => void }

if (typeof w === 'object' && w !== null) {
    (w as { runANonExistentMethod: Function }).runANonExistentMethod();
}