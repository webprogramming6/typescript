const car: { type: string, model: string, year: number } = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
};

console.log(car)

//Optional
const car2: { type: string, model?: string, year: number } = {
    type: "Toyota",
    year: 2009
};

console.log(car2)