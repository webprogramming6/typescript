interface Rectangle {
    height: number,
    width: number
}

interface Color extends Rectangle {
    colors: String
}
const rectangle: Rectangle = {
    height: 20,
    width: 10
};

const color: Color = {
    height: 20,
    width: 10,
    colors: "Pink"
}

console.log(rectangle)
console.log(color)