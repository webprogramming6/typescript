const graph: [x: number, y: number] = [55.2, 41.3];
console.log(graph)

//Destructuring Tuples
const graph2: [number, number] = [55.2, 41.3];
const [num1, num2] = graph2;
console.log(num1)
console.log(num2)